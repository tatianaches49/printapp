﻿using System;
using PrintLib; // подключение библиотеки

namespace PrintApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1.Square(); //отрисовка квадарата 5х10
            Class1.Elipse(); // отрисовка круглишка
            Class1.Pyramid(); // отрисовка пирамидки
            Class1.Romb(); // отрисовка ромба
            Class1.Tree(); // отрисовка ёлочки
        }
    }
}
